package main;

import graph.GraphController;
import javafx.application.Application;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    private GraphController graphController;

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainView.fxml"));

        primaryStage.setTitle("Graphy");
        primaryStage.setScene(new Scene(loader.load(), 1280, 720));
        primaryStage.getIcons().add(new Image("/resources/icon.png"));
        graphController = loader.getController();
        graphController.initGraph(primaryStage);

        primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);

        primaryStage.show();

    }

    private <T extends Event> void closeWindowEvent(T t) {

        if(!graphController.isSaved())
            graphController.onNotSaved();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
