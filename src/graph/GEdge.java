package graph;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.jgrapht.Graph;

import java.io.IOException;

public class GEdge extends GraphItem {

    private static final int RADIUS = 30;

    private double weight;

    private Color color;

    private GVertex vertex1;
    private GVertex vertex2;

    private Point2D offset;

    private Graph<GVertex, GEdge> graph;

    GEdge(Graph<GVertex, GEdge> graph, GVertex vertex1, GVertex vertex2, double weight, Color color) {

        this.graph = graph;
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.weight = weight;
        this.color = color;

        offset = Point2D.ZERO;

        if(graph.getType().isAllowingSelfLoops() && vertex1.equals(vertex2)) {

            offset = offset.add(0, GVertex.RADIUS * 2 + RADIUS);

        }

        if(!graph.getType().isDirected()) {

            if ( graph.getType().isAllowingMultipleEdges()) {

                offset = offset.add(0, (graph.getAllEdges(vertex1, vertex2).size() - 1) * RADIUS * 2);
            }

        } else {

            int nodes = graph.getAllEdges(vertex1, vertex2).size() + graph.getAllEdges(vertex2, vertex1).size() - 1;

            offset = offset.add(0, nodes * RADIUS * 2 + RADIUS);

        }
    }

    public void draw(GraphicsContext gc, Point2D offset) {

        Point2D midpoint = getMidPoint().add(offset);

        drawCurve(gc, midpoint, offset);

        gc.setFill(color);

        gc.fillOval(midpoint.getX() - RADIUS + this.offset.getX(), midpoint.getY() - RADIUS + this.offset.getY(), RADIUS * 2, RADIUS * 2);
        gc.strokeOval(midpoint.getX() - RADIUS + this.offset.getX(), midpoint.getY() - RADIUS + this.offset.getY(), RADIUS * 2, RADIUS * 2);

        if (graph.getType().isDirected()) {

            Point2D direction = vertex2.getPosition().subtract(getPosition()).normalize();

            Point2D triangleCenter = getPosition().add(offset).add(direction.multiply(RADIUS + 5));

            double radians = Math.atan2(direction.getY(), direction.getX());

            gc.setFill(Color.BLACK);
            gc.fillPolygon(
                    new double[]{triangleCenter.getX() + Math.cos(radians) * 5, triangleCenter.getX() + Math.cos(radians + 2*Math.PI/3) * 5, triangleCenter.getX() + Math.cos(radians + 4*Math.PI/3) * 5},
                    new double[]{triangleCenter.getY() + Math.sin(radians) * 5, triangleCenter.getY() + Math.sin(radians + 2*Math.PI/3) * 5, triangleCenter.getY() + Math.sin(radians + 4*Math.PI/3) * 5},
                    3);
        }

        gc.setFill(Color.BLACK);
        gc.fillText("" + weight, midpoint.getX() + this.offset.getX(), midpoint.getY() + this.offset.getY());

    }

    @Override
    public void drawSelected(GraphicsContext gc, Point2D offset) {

        gc.setStroke(Color.BLUE);

        Point2D midpoint = getMidPoint();

        gc.strokeOval(midpoint.getX() - RADIUS + offset.getX() + this.offset.getX(), midpoint.getY() - RADIUS + offset.getY() + this.offset.getY(), RADIUS * 2, RADIUS * 2);

    }

    private void drawCurve(GraphicsContext gc, Point2D midPoint, Point2D screenOffset) {

        gc.setFill(Color.BLUE);

        Point2D v1 = vertex1.getPosition().add(screenOffset);
        Point2D v2 = vertex2.getPosition().add(screenOffset);

        double x1 = 2*(midPoint.getX() + offset.getX()) - v1.getX()/2 - v2.getX()/2;
        double y1 = 2*(midPoint.getY() + offset.getY()) - v1.getY()/2 - v2.getY()/2;

        gc.beginPath();

        gc.moveTo(v1.getX(), v1.getY());
        gc.quadraticCurveTo(x1, y1, v2.getX(), v2.getY());

        gc.stroke();

    }

    @Override
    public boolean contains(Point2D point) {
        return getMidPoint().add(offset).distance(point) < RADIUS;
    }

    @Override
    public Point2D getPosition() {
        return getMidPoint().add(offset);
    }

    @Override
    public boolean isOnScreen(Rectangle2D screen) {
        return vertex1.isOnScreen(screen) || vertex2.isOnScreen(screen);
    }

    private Point2D getMidPoint() {
        return new Point2D((vertex1.getPosition().getX() + vertex2.getPosition().getX()) / 2,
                (vertex1.getPosition().getY() + vertex2.getPosition().getY()) / 2);
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public GVertex getVertex1() {
        return vertex1;
    }

    public GVertex getVertex2() {
        return vertex2;
    }

    public void setOffset(Point2D position) {
        this.offset = position.subtract(getMidPoint());
    }

    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        weight = stream.readDouble();
        offset = new Point2D(stream.readDouble(), stream.readDouble());
        vertex1 = (GVertex) stream.readObject();
        vertex2 = (GVertex) stream.readObject();
        graph = (Graph<GVertex, GEdge>) stream.readObject();
        color = new Color(stream.readDouble(), stream.readDouble(), stream.readDouble(), 1);

    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeDouble(weight);
        stream.writeDouble(offset.getX());
        stream.writeDouble(offset.getY());
        stream.writeObject(vertex1);
        stream.writeObject(vertex2);
        stream.writeObject(graph);
        stream.writeDouble(color.getRed());
        stream.writeDouble(color.getGreen());
        stream.writeDouble(color.getBlue());

    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
