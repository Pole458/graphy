package graph;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.io.ObjectStreamException;

public class GVertex extends GraphItem {

    private String name;

    private Point2D position;

    private Color color;

    private int shape;

    public static final int RADIUS = 50;

    public static final int CIRCLE = 0;
    public static final int SQUARE = 1;
    public static final int DIAMOND = 2;
    public static final int RHOMBUS = 3;
    public static final int TRIANGLE = 4;

    GVertex(String name, double x, double y, Color color, int shape) {
        this.name = name;
        position = new Point2D(x, y);
        this.shape = shape;
        this.color = color;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean isOnScreen(Rectangle2D screen) {

        return screen.contains(position);

    }

    @Override
    public void drawSelected(GraphicsContext gc, Point2D offset) {

        gc.setStroke(Color.BLUE);

        Point2D screenPos = getPosition().add(offset);

        switch (shape) {
            case CIRCLE:

                gc.strokeOval(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);

                break;
            case SQUARE:

                gc.strokeRect(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);

                break;
            case DIAMOND:

                gc.strokePolygon(new double[]{screenPos.getX(), screenPos.getX() - RADIUS, screenPos.getX(), screenPos.getX() + RADIUS},
                        new double[]{screenPos.getY() + RADIUS, screenPos.getY(), screenPos.getY() - RADIUS, screenPos.getY()},
                        4);

                break;
            case RHOMBUS:

                gc.strokePolygon(
                        new double[]{screenPos.getX() + RADIUS * 0.9, screenPos.getX() - RADIUS * 1.1, screenPos.getX() - RADIUS * 0.9, screenPos.getX() + RADIUS * 1.1},
                        new double[]{screenPos.getY() + RADIUS, screenPos.getY() + RADIUS, screenPos.getY() - RADIUS, screenPos.getY() - RADIUS},
                        4);
                break;

            case TRIANGLE:
                gc.strokePolygon(
                        new double[]{screenPos.getX(), screenPos.getX() + RADIUS * Math.cos(Math.PI / 6), screenPos.getX() + RADIUS * Math.cos(Math.PI * 5 / 6)},
                        new double[]{screenPos.getY() - RADIUS, screenPos.getY() + RADIUS * Math.sin(Math.PI / 6), screenPos.getY() + RADIUS * Math.sin(Math.PI * 5 / 6)},
                        3);
                break;
        }
    }

    public String getName() {
        return name;
    }

    public boolean contains(Point2D point) {

        switch (shape) {
            case CIRCLE:
            case TRIANGLE:

                double x = getPosition().getX() - point.getX();
                double y = getPosition().getY() - point.getY();

                return x * x + y * y < RADIUS * RADIUS;

            case SQUARE:
            case RHOMBUS:

                return point.getX() < getPosition().getX() + RADIUS  &&
                        point.getX() > getPosition().getX() - RADIUS &&
                        point.getY() < getPosition().getY() + RADIUS &&
                        point.getY() > getPosition().getY() - RADIUS;

            case DIAMOND:

                x = Math.abs(getPosition().getX() - point.getX());
                y = Math.abs(getPosition().getY() - point.getY());

                return x + y < RADIUS;

        }

        return false;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(Point2D point) {
        position = point;
    }

    public void draw(GraphicsContext gc, Point2D offset) {

        gc.setFill(getColor());

        Point2D screenPos = getPosition().add(offset);

        switch (shape) {
            case CIRCLE:

                gc.fillOval(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);
                gc.strokeOval(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);

                break;
            case SQUARE:

                gc.fillRect(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);
                gc.strokeRect(screenPos.getX() - RADIUS, screenPos.getY() - RADIUS, RADIUS * 2, RADIUS * 2);

                break;
            case DIAMOND:

                gc.fillPolygon(new double[]{screenPos.getX(), screenPos.getX() - RADIUS, screenPos.getX(), screenPos.getX() + RADIUS},
                new double[]{screenPos.getY() + RADIUS, screenPos.getY(), screenPos.getY() - RADIUS, screenPos.getY()},
                4);
                gc.strokePolygon(new double[]{screenPos.getX(), screenPos.getX() - RADIUS, screenPos.getX(), screenPos.getX() + RADIUS},
                        new double[]{screenPos.getY() + RADIUS, screenPos.getY(), screenPos.getY() - RADIUS, screenPos.getY()},
                        4);

                break;
            case RHOMBUS:

                gc.fillPolygon(
                        new double[]{screenPos.getX() + RADIUS * 0.9, screenPos.getX() - RADIUS * 1.1, screenPos.getX() - RADIUS * 0.9, screenPos.getX() + RADIUS * 1.1},
                        new double[]{screenPos.getY() + RADIUS, screenPos.getY() + RADIUS, screenPos.getY() - RADIUS, screenPos.getY() - RADIUS},
                        4);
                gc.strokePolygon(
                        new double[]{screenPos.getX() + RADIUS * 0.9, screenPos.getX() - RADIUS * 1.1, screenPos.getX() - RADIUS * 0.9, screenPos.getX() + RADIUS * 1.1},
                        new double[]{screenPos.getY() + RADIUS, screenPos.getY() + RADIUS, screenPos.getY() - RADIUS, screenPos.getY() - RADIUS},
                        4);
                break;

            case TRIANGLE:

                gc.fillPolygon(
                        new double[]{screenPos.getX(), screenPos.getX() + RADIUS * Math.cos(Math.PI / 6), screenPos.getX() + RADIUS * Math.cos(Math.PI * 5 / 6)},
                        new double[]{screenPos.getY() - RADIUS, screenPos.getY() + RADIUS * Math.sin(Math.PI / 6), screenPos.getY() + RADIUS * Math.sin(Math.PI * 5 / 6)},
                        3);

                gc.strokePolygon(
                        new double[]{screenPos.getX(), screenPos.getX() + RADIUS * Math.cos(Math.PI / 6), screenPos.getX() + RADIUS * Math.cos(Math.PI * 5 / 6)},
                        new double[]{screenPos.getY() - RADIUS, screenPos.getY() + RADIUS * Math.sin(Math.PI / 6), screenPos.getY() + RADIUS * Math.sin(Math.PI * 5 / 6)},
                        3);
                break;
        }

        gc.setFill(Color.BLACK);
        gc.fillText(name, screenPos.getX(), screenPos.getY());

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(Color color) {
       this.color = color;
    }

    public Color getColor() {
        return color;
    }

    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        this.name = stream.readUTF();
        this.position = new Point2D(stream.readDouble(), stream.readDouble());
        this.color = new Color(stream.readDouble(), stream.readDouble(), stream.readDouble(), 1);
        this.shape = stream.readInt();
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {

        stream.writeUTF(name);
        // Position
        stream.writeDouble(position.getX());
        stream.writeDouble(position.getY());
        // Color
        stream.writeDouble(color.getRed());
        stream.writeDouble(color.getGreen());
        stream.writeDouble(color.getBlue());

        stream.writeInt(shape);
    }

    private void readObjectNoData() throws ObjectStreamException {
        this.name = "Vertex";
        position = new Point2D(100, 100);
    }

    public int getShape() {
        return shape;
    }

    public void setShape(int shape) {
        this.shape = shape;
    }

    public static int shapeFromString(String s) {
        switch (s) {
            case "Circle":
                return CIRCLE;
            case "Square":
                return SQUARE;
            case "Diamond":
                return DIAMOND;
            case "Rhombus":
                return RHOMBUS;
            case "Triangle":
                return TRIANGLE;
        }

        return CIRCLE;
    }

}
