package graph;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;

import java.io.Serializable;

abstract class GraphItem implements Serializable {

    public abstract void draw(GraphicsContext gc, Point2D offset);

    public abstract void drawSelected(GraphicsContext gc, Point2D offset);

    public abstract boolean contains(Point2D point);

    public abstract Point2D getPosition();

    public abstract boolean isOnScreen(Rectangle2D screen);
}
