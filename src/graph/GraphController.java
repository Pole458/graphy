package graph;

import graph.dialogs.AddEdgeDialogController;
import graph.dialogs.AddVertexDialogController;
import graph.inspectors.GEdgeInspectorController;
import graph.inspectors.GVertexInspectorController;
import graph.inspectors.GridInspectorController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import graph.dialogs.NewGraphDialogController;
import org.jgrapht.Graph;
import org.jgrapht.graph.builder.GraphTypeBuilder;

import java.io.*;
import java.util.Optional;

import static graph.GVertex.CIRCLE;

public class GraphController {

    @FXML
    public MenuItem saveButton;
    @FXML
    public MenuItem saveAsButton;
    @FXML
    public MenuItem addVertexButton;
    @FXML
    public MenuItem addEdgeButton;
    @FXML
    public MenuItem gridButton;
    @FXML
    private BorderPane mainBorderPane;
    @FXML
    public CheckBox directedEdges;
    @FXML
    public CheckBox selfLoops;
    @FXML
    public CheckBox multipleEdges;
    @FXML
    public Label verticesTF;
    @FXML
    public Label edgesTF;

    private GraphView view;

    private Graph<GVertex, GEdge> model;

    private Stage primaryStage;

    private String savePath;
    private boolean saved = true;

    private GraphItem selectedItem = null;
    private Point2D dragOffset = null;

    private Node graphInspector;

    private Node gVertexInspector;
    private GVertexInspectorController gVertexInspectorController;

    private Node gArcInspector;
    private GEdgeInspectorController gEdgeInspectorController;

    private Node gridInspector;

    public void initGraph(Stage primaryStage) {

        this.primaryStage = primaryStage;

        view = new GraphView(this);

        mainBorderPane.setCenter(view);
        graphInspector = mainBorderPane.getRight();

    }

    public void initNew(boolean directSelected, boolean selfLoopsSelected, boolean multipleEdgesSelected) {

        primaryStage.setTitle("Graphy - unsaved*");

        savePath = null;

        Graph<GVertex, GEdge> model;

        if(directSelected) {

            model = GraphTypeBuilder.<GVertex, GEdge> directed().allowingMultipleEdges(multipleEdgesSelected)
                    .allowingSelfLoops(selfLoopsSelected).weighted(true).edgeClass(GEdge.class).buildGraph();

        } else {

            model = GraphTypeBuilder.<GVertex, GEdge> undirected().allowingMultipleEdges(multipleEdgesSelected)
                    .allowingSelfLoops(selfLoopsSelected).weighted(true).edgeClass(GEdge.class).buildGraph();
        }

        GVertex node1 = new GVertex("Vertex 1", 100, 100, Color.RED, CIRCLE);
        GVertex node2 = new GVertex("Vertex 2", 300, 100, Color.RED, CIRCLE);

        model.addVertex(node1);
        model.addVertex(node2);

        model.addEdge(node1, node2, new GEdge(model, node1, node2, 1, Color.WHITE));

//        Stress test
//        GVertex vertices[] = new GVertex[1000];
//        for(int i = 0; i < vertices.length; i++) {
//            vertices[i] = new GVertex(String.format("Vertex %d", i), (100 * i) % 1000, 100 * (i / 10), Color.RED, i % 5);
//            model.addVertex(vertices[i]);
//        }
//
//        int s, t;
//        for (GVertex ignored : vertices) {
//            s = (int) (Math.random() * vertices.length);
//            t = (int) (Math.random() * vertices.length);
//            model.addEdge(vertices[s], vertices[t], new GEdge(model, vertices[s], vertices[t], 1, Color.WHITE));
//        }
//
        setModel(model);

    }

    private void setModel(Graph<GVertex, GEdge> model) {

        this.model = model;
        view.setModel(model);

        selectedItem = null;
        if (saveButton.isDisable()) {
            saveButton.setDisable(false);
            saveAsButton.setDisable(false);
            addVertexButton.setDisable(false);
            addEdgeButton.setDisable(false);
            gridButton.setDisable(false);
        }

        verticesTF.setText(String.valueOf(model.vertexSet().size()));
        edgesTF.setText(String.valueOf(model.edgeSet().size()));

        directedEdges.setSelected(model.getType().isDirected());
        selfLoops.setSelected(model.getType().isAllowingSelfLoops());
        multipleEdges.setSelected(model.getType().isAllowingMultipleEdges());


        if(view.getOnMousePressed() == null)
            view.setOnMousePressed(this::onMousePressed);
        if(view.getOnMouseDragged() == null)
            view.setOnMouseDragged(this::onMouseDragged);
        if(view.getOnMouseReleased() == null)
            view.setOnMouseReleased(this::onMouseReleased);

    }

    public void onNew() {

        if(!saved)
            onNotSaved();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("dialogs/NewGraphDialog.fxml"));

        try {
            Parent parent = loader.load();

            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.getIcons().add(new Image("/resources/icon.png"));
            stage.setTitle("Select Graph Properties");
            stage.initModality(Modality.APPLICATION_MODAL);

            loader.<NewGraphDialogController>getController().initData(this, stage);

            stage.setScene(scene);
            stage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onOpen() {

        if(!saved)
            onNotSaved();

        FileChooser fileChooser = new FileChooser();

        //Set extension filter for gry files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Graphy files (*.gry)", "*.gry");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show onOpen file dialog
        File selectedDirectory = fileChooser.showOpenDialog(primaryStage);

        if(selectedDirectory != null) {

            try {

                FileInputStream fileIn = new FileInputStream(selectedDirectory.getAbsolutePath());
                savePath = selectedDirectory.getAbsolutePath();
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                @SuppressWarnings("unchecked") Graph<GVertex, GEdge> model = (Graph<GVertex, GEdge>) objectIn.readObject();
                objectIn.close();

                setModel(model);

                primaryStage.setTitle("Graphy - [" + savePath + "]");

            } catch (IOException | ClassNotFoundException ex) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Graphy");
                alert.setHeaderText("Ops! Could not load the file.");
                alert.setContentText("The file could be corrupted and can't be loaded.");

                alert.showAndWait();

            }

        }

    }

    public void onSave() {

        if(savePath == null) {

            savePath = askSavePath();
        }

        if(savePath != null) {
            save(savePath);
            primaryStage.setTitle("Graphy - [" + savePath + "]");
        }

    }

    public void onSaveAs() {

        String newSafePath = askSavePath();

        if(newSafePath != null) {
            savePath = newSafePath;
            onSave();
        }

    }

    private String askSavePath() {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter for gry files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Graphy files (*.gry)", "*.gry");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedDirectory = fileChooser.showSaveDialog(primaryStage);

        if(selectedDirectory != null)
            return selectedDirectory.getAbsolutePath();
        else
            return null;
    }

    public void onAddVertex() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("dialogs/AddVertexDialog.fxml"));

        try {
            Parent parent = loader.load();

            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.getIcons().add(new Image("/resources/icon.png"));
            stage.setTitle("Add Vertex");
            stage.initModality(Modality.APPLICATION_MODAL);

            loader.<AddVertexDialogController>getController().SetGraphController(this, stage);

            stage.setScene(scene);
            stage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onAddEdge() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("dialogs/AddEdgeDialog.fxml"));

        try {

            Parent parent = loader.load();

            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.getIcons().add(new Image("/resources/icon.png"));
            stage.setTitle("Add Edge");
            stage.initModality(Modality.APPLICATION_MODAL);

            loader.<AddEdgeDialogController>getController().SetGraphController(this, stage, model.vertexSet());

            stage.setScene(scene);
            stage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addVertex(String name, Color color, int shape) {

        saved = false;

        double x = (view.getScreen().getMaxX() + view.getScreen().getMinX() ) / 2 ;
        double y = (view.getScreen().getMaxY() + view.getScreen().getMinY() ) / 2 ;

        model.addVertex(new GVertex(name, x, y, color, shape));

        verticesTF.setText(String.valueOf(model.vertexSet().size()));

    }

    public void addEdge(GVertex vertex1, GVertex vertex2, double weight, Color color) {

        saved = false;

        if(model.getType().isAllowingSelfLoops() || !vertex1.equals(vertex2)) {

            model.addEdge(vertex1, vertex2, new GEdge(model, vertex1, vertex2, weight, color));

            edgesTF.setText(String.valueOf(model.edgeSet().size()));

            if (selectedItem instanceof  GVertex)
                gVertexInspectorController.degreeLabel.setText(String.valueOf(model.degreeOf((GVertex) selectedItem)));

        }

    }

    private void onMousePressed(MouseEvent event) {

        Point2D point = new Point2D(event.getX(), event.getY());

        for(GVertex gVertex : model.vertexSet()) {

            if (gVertex.isOnScreen(view.getScreen()) && gVertex.contains(point.subtract(view.getOffset()))) {

                dragOffset = gVertex.getPosition().subtract(point);

                selectItem(gVertex);

                return;
            }

        }

        for( GEdge gEdge : model.edgeSet()) {

            if (gEdge.isOnScreen(view.getScreen()) && gEdge.contains(point.subtract(view.getOffset()))) {

                dragOffset = gEdge.getPosition().subtract(point);

                selectItem(gEdge);

                return;
            }
        }

        mainBorderPane.setRight(graphInspector);
        selectedItem = null;

        if(event.getButton() == MouseButton.PRIMARY)
            dragOffset = point.subtract(view.getOffset());

    }

    private void onMouseDragged(MouseEvent event) {

        if (event.getButton() == MouseButton.PRIMARY && dragOffset != null ) {

            if (selectedItem == null) {

                view.setOffset(new Point2D(event.getX(), event.getY()).subtract(dragOffset));

            } else {

                Point2D pos;

                if (view.getGrid()) {

                    pos = new Point2D(((int)(event.getX() + dragOffset.getX()) / view.getGridW()) * view.getGridW(),
                            ((int)(event.getY() + dragOffset.getY()) / view.getGridH()) * view.getGridH());

                } else {

                    pos = new Point2D(event.getX(), event.getY()).add(dragOffset);
                }

                saved = false;

                if( selectedItem instanceof GVertex) {

                    ((GVertex)selectedItem).setPosition(pos);

                } else if( selectedItem instanceof GEdge) {

                    ((GEdge)selectedItem).setOffset(pos);

                }
            }
        }
    }

    private void onMouseReleased(MouseEvent event) {

        if (event.getButton() == MouseButton.SECONDARY && selectedItem instanceof GVertex && dragOffset != null) {

            for (GVertex gVertex : model.vertexSet()) {

                if (gVertex.isOnScreen(view.getScreen()) && gVertex.contains(new Point2D(event.getX(), event.getY()).subtract(view.getOffset()))) {

                    addEdge((GVertex) selectedItem, gVertex, 1, Color.WHITE);
                    break;
                }
            }
        }

        dragOffset = null;

    }

    private void save(String path) {

        try {

            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(model);
            objectOut.close();

            saved = true;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void removeVertex(GVertex vertex) {

        saved = false;

        model.removeVertex(vertex);
        mainBorderPane.setRight(graphInspector);
        if(selectedItem.equals(vertex))
            selectedItem = null;

        verticesTF.setText(String.valueOf(model.vertexSet().size()));
        edgesTF.setText(String.valueOf(model.edgeSet().size()));

    }

    public void removeEdge(GEdge edge) {

        saved = false;

        model.removeEdge(edge);
        mainBorderPane.setRight(graphInspector);
        if(selectedItem.equals(edge))
            selectedItem = null;

        edgesTF.setText(String.valueOf(model.edgeSet().size()));
    }

    public GraphItem getSelectedItem() {
        return selectedItem;
    }

    public void onGridToggle() {

        if (gridInspector == null) {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("inspectors/GridInspector.fxml"));

            try {
                gridInspector = loader.load();
                GridInspectorController gridInspectorController = loader.getController();
                gridInspectorController.initData(view);

            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (mainBorderPane.getRight() != gridInspector)
            mainBorderPane.setRight(gridInspector);

    }

    public Graph<GVertex,GEdge> getModel() {
        return model;
    }

    public void selectItem(GraphItem item) {

        selectedItem = item;

        if (selectedItem instanceof GVertex) {

            if (gVertexInspector == null) {

                FXMLLoader loader = new FXMLLoader(getClass().getResource("inspectors/GVertexInspector.fxml"));

                try {
                    gVertexInspector = loader.load();
                    gVertexInspectorController = loader.getController();
                    gVertexInspectorController.setGraphController(this);

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

            if (mainBorderPane.getRight() != gVertexInspector)
                mainBorderPane.setRight(gVertexInspector);
            gVertexInspectorController.setGNode((GVertex) selectedItem);

        } else if (selectedItem instanceof GEdge) {

            if (gArcInspector == null) {

                FXMLLoader loader = new FXMLLoader(getClass().getResource("inspectors/GEdgeInspector.fxml"));

                try {
                    gArcInspector = loader.load();
                    gEdgeInspectorController = loader.getController();
                    gEdgeInspectorController.setGraphController(this);

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

            if (mainBorderPane.getRight() != gArcInspector)
                mainBorderPane.setRight(gArcInspector);
            gEdgeInspectorController.setGEdge((GEdge) selectedItem);

        }

        if(!selectedItem.isOnScreen(view.getScreen()))
            view.focus(selectedItem);
    }

    public boolean isSaved() {
        return saved;
    }

    public void onNotSaved() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("The Graph is not saved!");
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image("/resources/icon.png"));
        alert.setHeaderText("Would you like to save this graph before closing it?");
        alert.setContentText("Closing the graph without saving it will discard every changes made.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            onSave();
        }
    }

}
