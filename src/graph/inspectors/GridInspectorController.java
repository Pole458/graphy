package graph.inspectors;

import graph.GraphView;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class GridInspectorController {

    @FXML
    public ToggleButton toggleGrid;
    @FXML
    public TextField weightTF;
    @FXML
    public TextField heightTF;

    private GraphView view;

    public void initData(GraphView v) {

        this.view = v;

        toggleGrid.setSelected(view.getGrid());
        weightTF.setText(String.valueOf(view.getGridW()));
        heightTF.setText(String.valueOf(view.getGridH()));

        weightTF.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                int w = Math.min(100, Math.max(5, Integer.valueOf(weightTF.getText())));
                view.setGridW(w);
                weightTF.setText(String.valueOf(w));
            } catch (NumberFormatException ignored) {
                weightTF.setText(String.valueOf(view.getGridW()));
            }
        });

        heightTF.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                int h = Math.min(100, Math.max(5, Integer.valueOf(heightTF.getText())));
                view.setGridH(h);
                heightTF.setText(String.valueOf(h));
            } catch (NumberFormatException ignored) {
                heightTF.setText(String.valueOf(view.getGridH()));
            }
        });
    }

    public void onToggleGrid() {

        view.setGrid(toggleGrid.isSelected());

    }

}
