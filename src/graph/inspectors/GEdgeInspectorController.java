package graph.inspectors;

import graph.GEdge;
import graph.GraphController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;

public class GEdgeInspectorController {

    @FXML
    public Button vertexSource;
    @FXML
    public Button vertexTarget;
    @FXML
    public ColorPicker colorPicker;
    @FXML
    private TextField weightTF;

    private GEdge gEdge;

    private GraphController graphController;

    public void setGraphController(GraphController gc) {
        graphController = gc;
    }

    @FXML
    public void initialize() {
        weightTF.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                gEdge.setWeight(Double.valueOf(weightTF.getText()));
            } catch (NumberFormatException e) {
                weightTF.setText(String.valueOf(gEdge.getWeight()));
            }
        });
    }

    public void setGEdge(GEdge edge) {
        this.gEdge = edge;

        weightTF.setText(String.valueOf(gEdge.getWeight()));
        vertexSource.setText(gEdge.getVertex1().toString());
        vertexTarget.setText(gEdge.getVertex2().toString());
        colorPicker.setValue(gEdge.getColor());
    }

    public void onRemoveEdge() {
        graphController.removeEdge(gEdge);
    }

    public void onVertexS() {

        graphController.selectItem(gEdge.getVertex1());

    }

    public void onVertexT() {

        graphController.selectItem(gEdge.getVertex2());

    }

    public void onChangedColor() {

        gEdge.setColor(colorPicker.getValue());

    }
}
