package graph.inspectors;

import graph.GVertex;
import graph.GraphController;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import static graph.GVertex.*;

public class GVertexInspectorController {

    @FXML public ChoiceBox<String> shapeChoiceBox;
    @FXML public Label degreeLabel;
    @FXML private TextField nameTF;
    @FXML private ColorPicker colorPicker;

    private GVertex gVertex;

    private GraphController graphController;

    public void setGraphController(GraphController gc) {
        graphController = gc;

        nameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if(nameTF.getText().length() > 10) {
                gVertex.setName(nameTF.getText(0, 10));
                nameTF.setText(nameTF.getText(0, 10));
            } else
                gVertex.setName(nameTF.getText());
        });
    }

    public void setGNode(GVertex vertex) {

        this.gVertex = vertex;

        nameTF.setText(gVertex.getName());

        degreeLabel.setText(String.valueOf(graphController.getModel().degreeOf(gVertex)));

        colorPicker.setValue(gVertex.getColor());

        switch (gVertex.getShape()) {
            case CIRCLE:
                shapeChoiceBox.setValue("Circle");
                break;
            case SQUARE:
                shapeChoiceBox.setValue("Square");
                break;
            case DIAMOND:
                shapeChoiceBox.setValue("Diamond");
                break;
            case RHOMBUS:
                shapeChoiceBox.setValue("Rhombus");
                break;
            case TRIANGLE:
                shapeChoiceBox.setValue("Triangle");
                break;
        }

    }

    public void onColorChanged() {

       gVertex.setColor(colorPicker.getValue());

    }

    public void onShapeChanged() {
        switch (shapeChoiceBox.getValue()) {
            case "Circle":
                gVertex.setShape(CIRCLE);
                break;
            case "Square":
                gVertex.setShape(SQUARE);
                break;
            case "Diamond":
                gVertex.setShape(DIAMOND);
                break;
            case "Rhombus":
                gVertex.setShape(RHOMBUS);
                break;
            case "Triangle":
                gVertex.setShape(TRIANGLE);
                break;
        }

    }

    public void onRemoveVertex() {

        graphController.removeVertex(gVertex);

    }
}

