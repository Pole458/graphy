package graph.dialogs;

import graph.GraphController;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class NewGraphDialogController {

    private GraphController graphController;
    private Stage stage;

    @FXML
    public CheckBox directedEdges;
    @FXML
    public CheckBox selfLoops;
    @FXML
    public CheckBox multipleEdges;

    public void initData(GraphController graphController, Stage stage) {
        this.graphController = graphController;
        this.stage = stage;
    }

    public void OnCreate() {

        graphController.initNew(directedEdges.isSelected(), selfLoops.isSelected(), multipleEdges.isSelected());

        stage.close();

    }

    public void OnCancel() {
        stage.close();
    }

}
