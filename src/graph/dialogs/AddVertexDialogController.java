package graph.dialogs;

import graph.GVertex;
import graph.GraphController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class AddVertexDialogController {

    @FXML
    private ChoiceBox<String> shapeChoiceBox;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private TextField textViewName;

    private GraphController graphController;
    private Stage stage;

    public void SetGraphController(GraphController gc, Stage s) {
        graphController = gc;
        stage = s;
    }

    public void OnAddNodePressed() {

        graphController.addVertex(textViewName.getText(), colorPicker.getValue(), GVertex.shapeFromString(shapeChoiceBox.getValue()));
        stage.close();

    }

}
