package graph.dialogs;

import graph.GVertex;
import graph.GraphController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Set;

public class AddEdgeDialogController {

    @FXML public TextField weightTF;
    @FXML public ComboBox<GVertex> sourcesEdgesComboBox;
    @FXML public ComboBox<GVertex> targetsEdgesComboBox;
    @FXML public ColorPicker colorPicker;

    private GraphController graphController;
    private Stage stage;


    public void SetGraphController(GraphController gc, Stage s, Set<GVertex> vertices) {

        graphController = gc;
        stage = s;

        ObservableList<GVertex> options = FXCollections.observableArrayList(vertices);
        sourcesEdgesComboBox.setItems(options);
        targetsEdgesComboBox.setItems(options);

    }

    public void OnAddEdgePressed() {

        if(sourcesEdgesComboBox.getValue() != null && targetsEdgesComboBox.getValue() != null)
            try {
                double w = Double.valueOf(weightTF.getText());
                graphController.addEdge(sourcesEdgesComboBox.getValue(), targetsEdgesComboBox.getValue(), w, colorPicker.getValue());
                stage.close();
            } catch (NumberFormatException e) {
                weightTF.setText(String.valueOf(1));
            }
    }
}
