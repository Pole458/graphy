package graph;

import javafx.animation.AnimationTimer;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import org.jgrapht.Graph;

public class GraphView extends Pane {

    private final Canvas canvas;

    private Graph<GVertex, GEdge> model;

    private GraphController graphController;

    private Animator animator;

    private boolean grid = false;
    private int gridW = 50;
    private int gridH = 50;

    private Point2D offset = Point2D.ZERO;

    private Rectangle2D screen;

    GraphView(GraphController gc) {

        graphController = gc;

        canvas = new Canvas();
        getChildren().add(canvas);

        screen = new Rectangle2D(offset.getX(), offset.getY(), getWidth(), getHeight());

    }

    public void setModel(Graph<GVertex, GEdge> model) {
        this.model = model;

        if (animator == null) {
            animator = new Animator();
            animator.start();
        }

    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is deprecated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);

    }

    public boolean getGrid() {
        return grid;
    }

    public void setGrid(boolean grid) {
        this.grid = grid;
    }

    public int getGridW() {
        return gridW;
    }

    public int getGridH() {
        return gridH;
    }

    public void setGridW(int gridW) {
        this.gridW = gridW;
    }

    public void setGridH(int gridH) {
        this.gridH = gridH;
    }

    public void setOffset(Point2D offset) {
        this.offset = offset;
    }

    public Point2D getOffset() {
        return offset;
    }

    public Rectangle2D getScreen() {
        return screen;
    }

    public void focus(GraphItem item) {

        offset = Point2D.ZERO.subtract(item.getPosition().subtract(getWidth() / 2, getHeight() / 2));

    }

    private class Animator extends AnimationTimer {

        private final GraphicsContext gc;

        Animator() {

            gc = canvas.getGraphicsContext2D();
            gc.setStroke(Color.BLACK);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setTextBaseline(VPos.CENTER);
        }

        @Override
        public void handle(long now) {

            gc.clearRect(0,0,getWidth(), getHeight());
            gc.setFill(Color.LIGHTGRAY);
            gc.fillRect(0, 0, getWidth(), getHeight());

            //Draw grid
            if (grid) {
                gc.setStroke(Color.GRAY);
                for (double w = offset.getX() % gridW; w < canvas.getWidth(); w += gridW)
                    gc.strokeLine(w, 0, w, canvas.getHeight());
                for (double h = offset.getY() % gridH; h < canvas.getHeight(); h += gridH)
                    gc.strokeLine(0, h, canvas.getWidth(), h);
            }

            gc.setStroke(Color.BLACK);

            screen = new Rectangle2D(-offset.getX() - GVertex.RADIUS, -offset.getY() - GVertex.RADIUS,
                    getWidth() + GVertex.RADIUS * 2, getHeight() + GVertex.RADIUS * 2);


            // Draw all edges
            for (GEdge edge : model.edgeSet()) {

                if(edge.isOnScreen(screen))
                    edge.draw(gc, offset);

            }

            // Draw all nodes
            for (GVertex node : model.vertexSet()) {

                if(node.isOnScreen(screen))
                    node.draw(gc, offset);

            }

            gc.setLineWidth(4);

            if (graphController.getSelectedItem() != null && graphController.getSelectedItem().isOnScreen(screen))
                graphController.getSelectedItem().drawSelected(gc, offset);

            gc.setLineWidth(1);
        }
    }

}
